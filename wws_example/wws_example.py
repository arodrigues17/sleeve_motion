import config
import wws
import quaternion

import rospy
import math
import json
import time



banid= "ARdevice"
queue= "testing"
routing_key_arr= [".imu.sleeve",".display.response",".rawdata.sleeve",".button.sleeve"]
haptic_key= banid + '.haptic'
_quat_init_inv= None
sleeve_link_length= .45



def _quat_rotate(q,r):
	"""Rotates a quaternion object by another quaternion"""
	return quaternion.Quaternion(
		q.w * r.x + q.x * r.w + q.y * r.z - q.z * r.y,
        q.w * r.y - q.x * r.z + q.y * r.w + q.z * r.x,
        q.w * r.z + q.x * r.y - q.y * r.x + q.z * r.w,
        q.w * r.w - q.x * r.x - q.y * r.y - q.z * r.z
	)

def _quat_rotate_axis_angle(q, x, y, z, angle):
        """This function rotates a quaternion object """
        sincomp= math.sin(angle / 2.0)
        q *= quaternion.Quaternion(
            x * sincomp, y * sincomp,
            z * sincomp, math.cos(angle / 2.0)
        )
        return q

def haptic_msg(banid):
        """effects: grab, release, TODO"""
        msg= json.dumps({
            "from": "SleeveController "+banid,
            "to": banid,
            "time": int(round(time.time(), 3)*1000),
            "v": "V2",
            "type": "haptic",
            "mode": "effect library",
            "data": "uiactive"
            })
        return msg

def on_data(data, key):
	global client1

#	print(key)
#	print(data)

	if "imu.sleeve" in key:

		""" #general unneeded visual
		print("Full data {}/n".format(data))
		print("Wrist data{}/n".format(data["wrist"]["Qy"]))
		print ("Quaternion Y component data {}\n \n ".format(data["wrist"]["Qy"]))

		"""
		try:

			init_vector= [sleeve_link_length,0,0] #construct forearm link vector
			Qw, Qx, Qy, Qz= data['wrist']['Qw'], data['wrist']['Qx'], data['wrist']['Qy'], data['wrist']['Qz']  #Q assignment
			raw_orientation= quaternion.Quaternion(Qx, Qy, Qz, Qw)
			print("Raw: {}".format(raw_orientation))
			raw_orientation_z_corrected= _quat_rotate_axis_angle(raw_orientation, 0, 0, 1, math.radians(-90))
			print("Z corrected: {}".format(raw_orientation_z_corrected))
			corrected_orientation= _quat_rotate_axis_angle(raw_orientation_z_corrected, 1, 0, 0, math.radians(-30))
			print("X and Z Corrected: {}".format(corrected_orientation))

			if _quat_init_inv is None:
				_quat_init_inv= corrected_orientation.__invert__()

			rotated_orientation= _quat_rotate(_quat_init_inv,corrected_orientation)
			print('TEST {}'.format(rotated_orientation))


			"""
			#Convert 3D vector to R^4 dimention
			init_vector_q = quaternion.Quaternion(init_vector[0],init_vector[1] init_vector[2],0)
            #Rotate coordinate
            #v = q*uq*q'
            #q*uq
            q_uq = self._quat_rotate(rotated_orientation, init_vector_q)
            #(q*uq)*q'
            rotated_vector_q = self._quat_rotate(q_uq, rotated_orientation.__invert__())

            #Convert back to R^3 dimention
            rotated_vector = [rotated_vector_q.x, rotated_vector_q.y, rotated_vector_q.z]
            print("Rotated vector: {}".format(rotated_vector))
			"""


            if rospy.is_shutdown():
				client1.stop_consuming()

 		except:
			print('Error calculating rotation vector')
			"""





	if ".button.sleeve" in key:
		print ("stopped by button down event")
		client1.stop_consuming()






def main():
	#Subscribe example
	print ("Starting Stream")
	client1= wws.SleeveWws(banid, queue, on_data)
	client1.establish_connection()
	client1.register_channel(routing_key_arr)
	client1.start_consuming()
	client1.close_all()

	#Publish example
	print ("sending json msg to {}\n{}".format(haptic_key, haptic_msg(banid)))

	pub_client= wws.SleeveWws()
	pub_client.publish_msg(haptic_key, haptic_msg(banid))

if __name__ == '__main__':
	main()
