import wws
import json
import time

banid = "ARdevice"
queue = "testing"

routing_key_arr = [".imu.sleeve",".display.response",".rawdata.sleeve",".button.sleeve"]
haptic_key = banid + '.haptic'


def on_data(data, key):
	global client1

	print(key)
#	print(data)

	if "imu.sleeve" in key:
		print("Full data {}/n".format(data))
		print("Wrist data{}/n".format(data["wrist"]["Qy"]))
		print ("Quaternion Y component data {}\n \n ".format(data["wrist"]["Qy"]))

	if ".button.sleeve" in key:
		print ("stopped by button down event")
		client1.stop_consuming()

def haptic_msg_format(banid):
        """effects: grab, release, TODO"""
        msg1 = json.dumps({
            "from": "SleeveController "+banid,
            "to": banid,
            "time": int(round(time.time(), 3)*1000),
            "v": "V2",
            "type": "haptic",
			"mode": "waveform sequencer",
            "data": [    [ 14, -100, 14, 0, 0, 0, 0, 0 ],
                         [ 14, -100, 14, 0, 0, 0, 0, 0 ],
                         [ 14, -100, 14, 0, 0, 0, 0, 0 ],
						 [ 14, -100, 14, 0, 0, 0, 0, 0 ],
			             [ 14, -100, 14, 0, 0, 0, 0, 0 ],
						 [ 14, -100, 14, 0, 0, 0, 0, 0 ],
						 [ 14, -100, 14, 0, 0, 0, 0, 0 ],
						 [ 14, -100, 14, 0, 0, 0, 0, 0 ],
						 [ 14, -100, 14, 0, 0, 0, 0, 0 ],
					]
                    })
        return msg1

def main():
	#Subscribe example
	print ("Starting Stream")
	client1 = wws.SleeveWws(banid, queue, on_data)
	client1.establish_connection()
	client1.register_channel(routing_key_arr)
	client1.start_consuming()
	client1.close_all()

	#Publish example
	print ("sending json msg to {}\n{}".format(haptic_key, haptic_msg_format(banid)))

	pub_client = wws.SleeveWws()
	pub_client.publish_msg(haptic_key, haptic_msg_format(banid))

if __name__ == '__main__':
	main()
