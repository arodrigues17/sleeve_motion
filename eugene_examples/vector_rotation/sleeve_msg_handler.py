#!/usr/bin/env python
import rospy
import math
import json
import time

import config
import wws
import quaternion

class SleeveMsgHandler():
    """docstring for SleeveMsgHandler"""
    def __init__(self):
        self.wws_client =  wws.SleeveWws(config.BANID_DEFAULT,
                                                  config.QUEUE_DEFAULT,
                                                  self.on_data)
        self.routing_key_arr = [".imu.sleeve"]
        self.quat_init_inv = None # used to inverse initial quaternion (set to zero)
        self.sleeve_link_length = 0.45 # (m) (length of the initial vector)
        self.banid= config.BANID_DEFAULT
        self.wws_client.establish_connection()
        self.wws_client.register_channel(self.routing_key_arr)
        self.haptic_key = config.BANID_DEFAULT + '.haptic'
        pub_client = wws.SleeveWws()
        pub_client.publish_msg(self.haptic_key, self.haptic_msg_grab())

    def start_wws_stream(self):
        self.wws_client.start_consuming()

    def stop_wws_stream(self):
        self.wws_client.stop_consuming()

    def close_wws(self):
        self.wws_client.close_all()

    def _quat_rotate(self, q, r):
        """This function rotates a quaternion object by another quaternion object"""
        return quaternion.Quaternion(
            q.w * r.x + q.x * r.w + q.y * r.z - q.z * r.y,
            q.w * r.y - q.x * r.z + q.y * r.w + q.z * r.x,
            q.w * r.z + q.x * r.y - q.y * r.x + q.z * r.w,
            q.w * r.w - q.x * r.x - q.y * r.y - q.z * r.z
        )

    def _quat_rotate_axis_angle(self, q, x, y, z, angle):
        """This function rotates a quaternion object """
        sincomp = math.sin(angle / 2.0)
        q *= quaternion.Quaternion(
            x * sincomp, y * sincomp,
            z * sincomp, math.cos(angle / 2.0)
        )
        return q

    def haptic_msg_grab(self):
            """effects: uiactive, release, TODO"""
            msg = json.dumps({
                "from": "SleeveController "+self.banid,
                "to": self.banid,
                "time": int(round(time.time(), 3)*1000),
                "v": "V2",
                "type": "haptic",
                "mode": "waveform sequencer",
                "data": [[3, 3, 3, 3, 3, 3, 3, 3 ],[0, 0, 0, 0, 0, 0, 0, 0 ],[0, 0, 0, 0, 0, 0, 0, 0 ],[2, 2, 2, 2, 2, 2, 2, 2 ],[0, 0, 0, 0, 0, 0, 0, 0 ],[0, 0, 0, 0, 0, 0, 0, 0 ],[1, 1, 1, 1, 1, 1, 1, 1 ],[0, 0, 0, 0, 0, 0, 0, 0 ],[0, 0, 0, 0, 0, 0, 0, 0 ]]})
            return msg
    def on_data(self, data, key):
        """"""
        if "imu.sleeve" in key:
            try:
                # Construct forearm link vector
                init_vector = [self.sleeve_link_length, 0, 0]

                Qw, Qx, Qy, Qz = data['wrist']['Qw'], data['wrist']['Qx'], data['wrist']['Qy'], data['wrist']['Qz']

                raw_orientation = quaternion.Quaternion(Qx, Qy, Qz, Qw)
                raw_orientation = self._quat_rotate_axis_angle(raw_orientation, 0, 0, 1, math.radians(-90)) # Correct the Z-Axis <- IC on PCB is rotated 90 deg
                raw_orientation = self._quat_rotate_axis_angle(raw_orientation, 1, 0, 0, math.radians(-30)) # Correct the X/Y bias

                if self.quat_init_inv is None:
                    self.quat_init_inv = raw_orientation.__invert__()

                rotated_orientation = self._quat_rotate(self.quat_init_inv, raw_orientation)

                #Convert 3D vector to R^4 dimention
                init_vector_q = quaternion.Quaternion(init_vector[0],
                                                      init_vector[1],
                                                      init_vector[2],
                                                      0)
                #Rotate coordinate
                #v = q*uq*q'
                #q*uq
                q_uq = self._quat_rotate(rotated_orientation, init_vector_q)
                #(q*uq)*q'
                rotated_vector_q = self._quat_rotate(q_uq, rotated_orientation.__invert__())

                #Convert back to R^3 dimention
                rotated_vector = [rotated_vector_q.x, rotated_vector_q.y, rotated_vector_q.z]
                print("Rotated vector: {}".format(rotated_vector))

                if rospy.is_shutdown():
                    self.wws_client.stop_consuming()
            except:
                print("Error calculating rotation vector")

def main():
    #Init msg handler
    SleeveMsgHandler()
#    wws_stream = SleeveMsgHandler()
#    wws_stream.start_wws_stream()
#    wws_stream.close_wws()

if __name__ == '__main__':
    main()
