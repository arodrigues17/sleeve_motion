import wws
import json
import time

banid = "ARdevice"
queue = "testing"

routing_key_arr = [".display.response",".rawdata.sleeve",".button.sleeve"]
haptic_key = banid + '.haptic'



def on_data(data, key):
	global client1

	if "button" in key:
		print ("vibrate time")
		pub_client = wws.SleeveWws()
		pub_client.publish_msg(haptic_key, haptic_msg_format(banid))
	#	client1.stop_consuming()

def haptic_msg_format(banid):
        """x"""
        msg = json.dumps({
            "from": "SleeveController "+banid,
            "to": banid,
            "time": int(round(time.time(), 3)*1000),
            "v": "V2",
            "type": "haptic",
            "mode": "effect library",
            "data": "celebration"
            })
        return msg

def main():
	#Subscribe example
	print ("Starting Stream")
	client1 = wws.SleeveWws(banid, queue, on_data)
	client1.establish_connection()
	client1.register_channel(routing_key_arr)
	client1.start_consuming()
	client1.close_all()

	#Publish example
	print ("sending json msg to {}\n{}".format(haptic_key, haptic_msg_format(banid)))



if __name__ == '__main__':
	main()
