#!/usr/bin/env python

import pika
import config
import json
import rospy

class SleeveWws:
    """
    Initialises a connection to WWS for either streaming or publishing data.

    Attributes:
        banid (float): Device banid name.
        queue (float): Stream queue name.
        data_callback (function): Callback funtion that will run every time there's a data_call.

    """
    def __init__(self, banid=None, queue=None, data_callback=None):
        self.banid = banid
        self.queue = queue

        if banid == None:
            self.banid = config.BANID_DEFAULT
        if queue == None:
            self.queue = config.QUEUE_DEFAULT

        self.data_callback = data_callback
        try:
            self.wws_server = rospy.get_param('~wws_server')

        except:
            print("No wws IP ROS parameter")
            self.wws_server = config.WWS_SERVER_DEFAULT

        self.connection_established = False

    def establish_connection(self):
        """
        The function establishes a connection with a WWS server.
        If using ROS, add the server ip as a rosparam named "wws_server".
        Else edit the default WWS_SERVER_DEFAULT in the config file.
        """

        credentials = pika.PlainCredentials(username=config.WWS_USER, password=config.WWS_PWD)

        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=self.wws_server,
            port=config.WWS_PORT,
            virtual_host=config.WWS_VIRTUAL_HOST,
            credentials=credentials))

        self.channel = self.connection.channel()
        self.connection_established = True

    def register_channel(self, routing_key_arr=None):
        """
        The function registers the connection and binds desired routing keys.

        Parameters:
            routing_key_arr (str list): List of routing keys to bind.
        """

        #Ensure desired queue is not currently declared.
        self.channel.queue_delete(self.queue)
        #Declare desired queue.
        self.channel.queue_declare(queue=self.queue, exclusive=True, durable=False)
        #Bind.
        if routing_key_arr != None:
            for key in routing_key_arr:
                print ("binding to: {}{}".format(self.banid,key))
                self.channel.queue_bind(exchange='egress_exchange',queue=self.queue,routing_key=self.banid + key)

        self.channel.basic_consume(self.data_call, queue=self.queue, no_ack=True)

    def start_consuming(self):
        """
        This is a blocking function call which starts consuming WWS data.
        """
        try:
            print ("started consuming")
            # Process events as long as consumers exist on this
            # channel, with idle timeout after 1 sec. Do not use the default
            # implementation of channel.start_consuming(), because in
            # pika v0.10.0 there is no thread-safe way to stop_consuming().
            while self.channel._consumer_infos and self.connection_established:
                self.channel.connection.process_data_events(time_limit=1)

        except:
            pass

    def stop_consuming(self):
        """
        The function stops consuming WWS data.
        """
        try:
            print ("stop consuming")
            self.channel.stop_consuming()
        except:
            pass

    def close_channel(self):
        """
        The function closes the channel.
        """
        print ("closing channel")
        self.channel.close()

    def close_connection(self):
        """
        The function closes the connection.
        """
        print ("closing connection")
        self.connection_established = False
        self.connection.close()

    def close_all(self):
        """
        Ask the msg-receive loop to stop processing messages. safe to call
        from another thread.
        """
        self.connection_established = False


    def unregister_channel(self):
        """
        The function unregisters the channel by deleting the data_queue.
        """
        self.channel.queue_delete(self.queue)

    def data_call(self, channel, method, properties, body):
        """
        The function loads json data and returns to data_callback function
        The data callback function that uses this class should have 2 parameters, "data" and "key"
        as the data (json) and method.routing_key will be passed to those params.
        """
        body_str = body.decode('ascii')
        data = json.loads(body_str)

        if self.data_callback:
            self.data_callback(data, method.routing_key)

    def publish_msg(self, routing_key, msg):
        """
        The function publishes JSON formatted msg to routing key of assigned connection
        """
        if self.connection_established == False:
            self.establish_connection()

        self.channel.basic_publish(
            exchange = 'ingress_exchange',
            routing_key = routing_key,
            body = msg)
