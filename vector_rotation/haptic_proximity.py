#!/usr/bin/env python
import rospy
import operator
import math
import json
import time
import vg
import numpy as np
#import vibrations

import config
import wws
import quaternion

class SleeveMsgHandler():
    """docstring for SleeveMsgHandler"""
    def __init__(self):
        self.wws_client =  wws.SleeveWws(config.BANID_DEFAULT, config.QUEUE_DEFAULT,self.on_data)
        self.routing_key_arr = [".imu.sleeve"]
        self.quat_init_inv = None # used to inverse initial quaternion (set to zero)
        self.sleeve_link_length = .215 # (m) (length of the initial vector)
        self.banid= config.BANID_DEFAULT
        self.wws_client.establish_connection()
        self.wws_client.register_channel(self.routing_key_arr)
        self.haptic_key = config.BANID_DEFAULT + '.haptic'



    def start_wws_stream(self):
        self.wws_client.start_consuming()

    def stop_wws_stream(self):
        self.wws_client.stop_consuming()

    def close_wws(self):
        self.wws_client.close_all()

    def _quat_rotate(self, q, r):
        """This function rotates a quaternion object by another quaternion object"""
        return quaternion.Quaternion(
            q.w * r.x + q.x * r.w + q.y * r.z - q.z * r.y,
            q.w * r.y - q.x * r.z + q.y * r.w + q.z * r.x,
            q.w * r.z + q.x * r.y - q.y * r.x + q.z * r.w,
            q.w * r.w - q.x * r.x - q.y * r.y - q.z * r.z
        )

    def _quat_rotate_axis_angle(self, q, x, y, z, angle):
        """This function rotates a quaternion object """
        sincomp = math.sin(angle / 2.0)
        q *= quaternion.Quaternion(
            x * sincomp, y * sincomp,
            z * sincomp, math.cos(angle / 2.0)
        )
        return q

    def haptic_msg(self):
            """effects: uiactive, release, TODO"""
            msg = json.dumps({
                "from": "SleeveController "+self.banid,
                "to": self.banid,
                "time": int(round(time.time(), 3)*1000),
                "v": "V2",
                "type": "haptic",
                "mode": "waveform sequencer",
                "data": self.magn})
            return msg

    def on_data(self, data, key):
        """"""
        if "imu.sleeve" in key:
            try:
                # Construct forearm link vector
                init_vector = [self.sleeve_link_length, 0, 0]

                Qw, Qx, Qy, Qz = data['wrist']['Qw'], data['wrist']['Qx'], data['wrist']['Qy'], data['wrist']['Qz']

                raw_orientation = quaternion.Quaternion(Qx, Qy, Qz, Qw)
                raw_orientation = self._quat_rotate_axis_angle(raw_orientation, 0, 0, 1, math.radians(-90)) # Correct the Z-Axis <- IC on PCB is rotated 90 deg
                raw_orientation = self._quat_rotate_axis_angle(raw_orientation, 1, 0, 0, math.radians(-30)) # Correct the X/Y bias

                if self.quat_init_inv is None:
                    self.quat_init_inv = raw_orientation.__invert__()

                rotated_orientation = self._quat_rotate(self.quat_init_inv, raw_orientation)

                #Convert 3D vector to R^4 dimention
                init_vector_q = quaternion.Quaternion(init_vector[0], init_vector[1], init_vector[2], 0)
                #Rotate coordinate
                #v = q*uq*q'
                #q*uq
                q_uq = self._quat_rotate(rotated_orientation, init_vector_q)
                #(q*uq)*q'
                rotated_vector_q = self._quat_rotate(q_uq, rotated_orientation.__invert__())

                #Convert back to R^3 dimention
                rotated_vector = [rotated_vector_q.x, rotated_vector_q.y, rotated_vector_q.z]
                print("Rotated vector: {}".format(rotated_vector))

                arbitrary_point = np.array([5,5,-5])
                haptic_11= np.array([rotated_vector_q.x, rotated_vector_q.y-.02,rotated_vector_q.z+.035])
                haptic_12= np.array([rotated_vector_q.x+0.02, rotated_vector_q.y-.02,rotated_vector_q.z-.0375])
                haptic_13= np.array([rotated_vector_q.x-.03, rotated_vector_q.y-.02,rotated_vector_q.z+.02])

                haptic_21= np.array([rotated_vector_q.x, rotated_vector_q.y-.115,rotated_vector_q.z+.045])
                haptic_22= np.array([rotated_vector_q.x+0.0275, rotated_vector_q.y-.115,rotated_vector_q.z-.045])
                haptic_23= np.array([rotated_vector_q.x-.04, rotated_vector_q.y-.115,rotated_vector_q.z+.035])

                haptic_31= np.array([rotated_vector_q.x, rotated_vector_q.y-.185,rotated_vector_q.z+.06])
                haptic_32= np.array([rotated_vector_q.x+.035, rotated_vector_q.y-.185,rotated_vector_q.z-.045])
                haptic_33= np.array([rotated_vector_q.x-.05, rotated_vector_q.y-.185,rotated_vector_q.z+.04])
                #print("Haptic 11: {}\nHaptic 21: {}\nHaptic 31: {}".format(haptic_11,haptic_21,haptic_31))

                distance_11= vg.euclidean_distance(haptic_11, arbitrary_point)
                distance_12= vg.euclidean_distance(haptic_12, arbitrary_point)
                distance_13= vg.euclidean_distance(haptic_13, arbitrary_point)

                distance_21= vg.euclidean_distance(haptic_21, arbitrary_point)
                distance_22= vg.euclidean_distance(haptic_22, arbitrary_point)
                distance_23= vg.euclidean_distance(haptic_23, arbitrary_point)

                distance_31= vg.euclidean_distance(haptic_31, arbitrary_point)
                distance_32= vg.euclidean_distance(haptic_32, arbitrary_point)
                distance_33= vg.euclidean_distance(haptic_33, arbitrary_point)
                print("Distance 11: {}\nDistance 12: {}\nDistance 13: {}\nDistance 21: {}\nDistance 22: {}\nDistance 23: {}\nDistance 31: {}\nDistance 32: {}\nDistance 33: {}\n"
                .format(distance_11, distance_12,distance_13,distance_21,distance_22,distance_23,distance_31,distance_32,distance_33))

                sorted_distance_list= np.array(sorted([distance_11, distance_12,distance_13,distance_21,distance_22,distance_23,distance_31,distance_32,distance_33]))
                print(sorted_distance_list)

                magnitude_11= list(sorted_distance_list).index(distance_11)
                magnitude_12= list(sorted_distance_list).index(distance_12)
                magnitude_13= list(sorted_distance_list).index(distance_13)

                magnitude_21= list(sorted_distance_list).index(distance_21)
                magnitude_22= list(sorted_distance_list).index(distance_22)
                magnitude_23= list(sorted_distance_list).index(distance_23)

                magnitude_31= list(sorted_distance_list).index(distance_31)
                magnitude_32= list(sorted_distance_list).index(distance_32)
                magnitude_33= list(sorted_distance_list).index(distance_33)


                self.magn= [magnitude_11, magnitude_12, magnitude_13, magnitude_21, magnitude_22, magnitude_23, magnitude_31, magnitude_32, magnitude_33]
                print(self.magn)

                for i in range(len(self.magn)):
                    if self.magn[i] == 0:
                        self.magn[i] = [1, 1, 1, 1, 1, 1, 1, 1 ]
                    if self.magn[i] == 1:
                        self.magn[i] = [2, 2, 2, 2, 2, 2, 2, 2 ]
                    if self.magn[i] == 2:
                        self.magn[i] = [64, 64, 64, 64, 64, 64, 64, 64 ]
                    if self.magn[i] == 3:
                        self.magn[i] = [65, 65, 65, 65, 65, 65, 65, 65 ]
                    if self.magn[i] == 4:
                        self.magn[i] = [66, 66, 66, 66, 66, 66, 66, 66 ]
                    if self.magn[i] == 5:
                        self.magn[i] = [67, 67, 67, 67, 67, 67, 67, 67 ]
                    if self.magn[i] == 6:
                        self.magn[i] = [68, 68, 68, 68, 68, 68, 68, 68 ]
                    if self.magn[i] == 7:
                        self.magn[i] = [69, 69, 69, 69, 69, 69, 69, 69 ]
                    if self.magn[i] == 8:
                        self.magn[i] = [0, 0, 0, 0, 0, 0, 0, 0 ]
                print(self.magn)

                pub_client = wws.SleeveWws()
                pub_client.publish_msg(self.haptic_key, self.haptic_msg())


                if rospy.is_shutdown():
                    self.wws_client.stop_consuming()
            except:
                print("Error calculating rotation vector")

def main():
    #Init msg handler
    wws_stream = SleeveMsgHandler()
    wws_stream.start_wws_stream()
    wws_stream.close_wws()

if __name__ == '__main__':
    main()
